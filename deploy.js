import fs from 'fs-extra';

async function deploy() {
    await fs
        .outputFile(
            '../../views/sites/platcab-query/platcab-query.blade.php',
            fs.readFileSync('./dist/index.html')
        )
        .catch(err => {
            console.error(err);
        });

    await fs.remove('../../../public/__platcab-query/').catch(err => {
        console.error(err);
    });

    await fs
        .copy('./dist/__platcab-query', '../../../public/__platcab-query')
        .catch(err => {
            console.error(err);
        });

    await fs
        .mkdir('../../../public/__platcab-query/assets');
}

deploy();